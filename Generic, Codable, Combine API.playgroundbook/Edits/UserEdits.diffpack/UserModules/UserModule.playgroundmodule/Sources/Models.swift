
import Foundation

public struct RandomFact: Codable {
    public var _id: String?
    public var __v: Int?
    public var user: String?
    public var text: String?
    public var updatedAt: String?
    public var createdAt: String?
    public var deleted: Bool?
    public var source: Source?
    public var status: Status?
    public var used: Bool?
    public var type: String?
    
    public enum Source: String, Codable {
        case user
        case api
    }
    
    public struct Status: Codable {
        public var verified: Bool?
        public var sentCount: Int?
    }
}


