import Combine

public enum APIError: Error {
    case internalError
    case serverError
    case parsingError
}

public protocol CatFactProvider {
    func randomFact(completion: @escaping((Result<RandomFact, APIError>) -> Void))
    // MARK: Combine
    func randomFact() -> AnyPublisher<RandomFact, APIError>
}

